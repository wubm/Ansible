#!/bin/bash

WORK_DIR=$(dirname $(readlink -f $0))

# Create new key
if [ ! -d $HOME/.ssh ]; then
    mkdir $HOME/.ssh
fi
if [ ! -f $HOME/.ssh/id_rsa ]; then
    ssh-keygen -f $HOME/.ssh/id_rsa -t rsa -N '' -b 4096
fi

# Add id_rsa_pub to Ansible
if [ -f $HOME/.ssh/id_rsa.pub ]; then
    key=`awk '{ print $2 }' $HOME/.ssh/id_rsa.pub`
    if [ -f $WORK_DIR/Ansible/roles/SSHsetting/files/id_ras.pub ]; then
        rm $WORK_DIR/Ansible/roles/SSHsetting/files/id_ras.pub
    fi
    cat $WORK_DIR/Ansible/roles/SSHsetting/files/admin_ras.pub >> $WORK_DIR/Ansible/roles/SSHsetting/files/id_ras.pub
    if [[ `grep $key $WORK_DIR/Ansible/roles/SSHsetting/files/id_ras.pub` ]]; then
        # double check, but it will not heppen!
        echo "Key exists"
    else
        cat $HOME/.ssh/id_rsa.pub >> $WORK_DIR/Ansible/roles/SSHsetting/files/id_ras.pub
    fi
else
    echo "id_rsa.pub not found. Please put the public key to ${HOME}/.ssh/id_rsa.pub"
fi

cd $WORK_DIR/Ansible
sed -i "s/^ansible_user=.*/ansible_user=${USER}/g" hosts
sed -i "s/^  remote_user:.*/  remote_user: ${USER}/g" init.yml
sed -i "s/^  remote_user:.*/  remote_user: ${USER}/g" SSHsetting.yml
sed -i "s/ansible_port=12321/ansible_port=22/g" hosts
ansible-playbook SSHsetting.yml --ask-become-pass --ask-pass
sed -i "s/ansible_port=22/ansible_port=12321/g" hosts
ansible-playbook init.yml --ask-become-pass

